//
//  vector3.hpp
//  Operations with Vectors
//
//  Created by Drink on 6/7/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef vector3_h
#define vector3_h

namespace dk_math{
    
    template<class T>
    class Vector3 {
    public:
        T x;
        T y;
        T z;
        
        Vector3(): Vector3(0, 0) {}
        Vector3(T t_x, T t_y, T t_z): x(t_x), y(t_y), z(t_z) {}
        Vector3(const Vector3 &vec): x(vec.x), y(vec.y), z(vec.z) {}
        ~Vector3() {}
        
        static Vector3<T> zeros(){
            Vector3<T> v(0,0,0);
            return v;
        }
        
        static Vector3<T> ones(){
            Vector3<T> v(1,1,1);
            return v;
        }
        
        static Vector3<T> up(){
            Vector3<T> v(0,1,0);
            return v;
        }
        
        static Vector3<T> left(){
            Vector3<T> v(-1,0,0);
            return v;
        }
        
        static Vector3<T> right(){
            Vector3<T> v(1,0,0);
            return v;
        }
        
        static Vector3<T> down(){
            Vector3<T> v(0,-1,0);
            return v;
        }
        
        static Vector3<T> front(){
            Vector3<T> v(0,0,1);
            return v;
        }
        
        static Vector3<T> back(){
            Vector3<T> v(0,0,-1);
            return v;
        }
        
        
        Vector3<T> & operator= ( const Vector3<T> &v ) = default;
        
        Vector3<T> operator+( const T s) const {
            return Vector3(x+s, y+s, z+s);
        }
        
        Vector3<T> operator-( const T s) const {
            return Vector3(x-s, y-s, z-s);
        }
        
        Vector3<T> operator*( const T s) const {
            return Vector3(x*s, y*s, z*s);
        }
        
        Vector3<T> operator/( const T s) const {
            if(s == 0) { throw "divided by 0"; }
            return Vector3(x/s, y/s, z/s);
        }
        
        Vector3<T> cross( const Vector3<T> v) const {
            T new_x = y * v.z - z * v.y;
            T new_y = z * v.x - x * v.z;
            T new_z = x * v.y - y * v.x;
            
            return Vector3(new_x, new_y, new_z);
        }
        
        T dot( const Vector3<T> v) const {
            return (x * v.x) + (y * v.y) + (z * v.z);
        }
        
        T magnitude() const {
            return sqrt(magnitude_square());
        }
        
        T magnitude_square() const {
            return x*x + y*y + z*z;
        }
        
        Vector3<T> unit() const{
            return *this / magnitude();
        }
        
        T angle(const Vector3<T> v) const {
            return acos(this->dot(v) / (this->magnitude() * v.magnitude() ));
        }
        
    };
    
    typedef Vector3<int> Vector3i;
    typedef Vector3<float> Vector3f;
}

#endif /* vector3_h */
