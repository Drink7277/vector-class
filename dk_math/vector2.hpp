//
//  vector2.hpp
//  Operations with Vectors
//
//  Created by Drink on 4/7/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef vector2_hpp
#define vector2_hpp

#include <stdio.h>

#include <math.h>

namespace dk_math{
    
    template<class T>
    class Vector2 {
    public:
        T x;
        T y;

        
        Vector2(): Vector2(0, 0) {}
        Vector2(T t_x, T t_y): x(t_x), y(t_y) {}
        Vector2(const Vector2 &vec): x(vec.x), y(vec.y) {}
        ~Vector2() {}
        
        static Vector2<T> zeros(){
            Vector2<T> v(0,0);
            return v;
        }
        
        static Vector2<T> ones(){
            Vector2<T> v(1,1);
            return v;
        }
        
        static Vector2<T> up(){
            Vector2<T> v(0,1);
            return v;
        }
        
        static Vector2<T> left(){
            Vector2<T> v(-1,0);
            return v;
        }
        
        static Vector2<T> right(){
            Vector2<T> v(1,0);
            return v;
        }
        
        static Vector2<T> down(){
            Vector2<T> v(0,-1);
            return v;
        }
        
        
        Vector2<T> & operator= ( const Vector2<T> &v ) = default;
        
        Vector2<T> operator+( const T s) const {
            return Vector2(x+s, y+s);
        }
        
        Vector2<T> operator-( const T s) const {
            return Vector2(x-s, y-s);
        }
        
        Vector2<T> operator*( const T s) const {
            return Vector2(x*s, y*s);
        }
        
        Vector2<T> operator/( const T s) const {
            if(s == 0) { throw "divided by 0"; }
            return Vector2(x/s, y/s);
        }
        
        T dot( const Vector2<T> v) const {
            return (x * v.x) + (y * v.y);
        }
        
        T magnitude() const {
            return sqrt(magnitude_square());
        }
        
        T magnitude_square() const {
            return x*x + y*y;
        }
        
        Vector2<T> unit() const{
            return *this / magnitude();
        }
        
        T direction() const{
            return atan2 (y,x);
        }
        
        T angle(const Vector2<T> v) const {
            return acos(this->dot(v) / (this->magnitude() * v.magnitude() ));
        }
        
    };
    
    
    typedef Vector2<int> Vector2i;
    typedef Vector2<float> Vector2f;
}


#endif /* vector2_hpp */
