//
//  vector2_test.hpp
//  Operations with Vectors
//
//  Created by Drink on 6/7/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef vector2_test_h
#define vector2_test_h

using namespace dk_math;

TEST_CASE("Vector2 Static Method"){
    
    WHEN("Vector2i"){
        
        SECTION("Vector zeros"){
            Vector2i v = Vector2i::zeros();
            
            REQUIRE( v.x == 0 );
            REQUIRE( v.y == 0 );
        }
        
        SECTION("Vector ones"){
            Vector2i v = Vector2i::ones();
            
            REQUIRE( v.x == 1 );
            REQUIRE( v.y == 1 );
        }
        
        
        
        SECTION("Vector up"){
            Vector2i v = Vector2i::up();
            
            REQUIRE( v.x == 0 );
            REQUIRE( v.y == 1 );
        }
        
        SECTION("Vector down"){
            Vector2i v = Vector2i::down();
            
            REQUIRE( v.x == 0 );
            REQUIRE( v.y == -1 );
        }
        
        SECTION("Vector Left"){
            Vector2i v = Vector2i::left();
            
            REQUIRE( v.x == -1 );
            REQUIRE( v.y == 0 );
        }
        
        SECTION("Vector Right"){
            Vector2i v = Vector2i::right();
            
            REQUIRE( v.x == 1 );
            REQUIRE( v.y == 0 );
        }
    }
    
    WHEN("Vector2f"){
        
        SECTION("Vector zeros"){
            Vector2f v = Vector2f::zeros();
            
            REQUIRE( v.x == 0 );
            REQUIRE( v.y == 0 );
        }
        
        SECTION("Vector ones"){
            Vector2f v = Vector2f::ones();
            
            REQUIRE( v.x == 1 );
            REQUIRE( v.y == 1 );
        }
        
        
        
        SECTION("Vector up"){
            Vector2f v = Vector2f::up();
            
            REQUIRE( v.x == 0 );
            REQUIRE( v.y == 1 );
        }
        
        SECTION("Vector down"){
            Vector2f v = Vector2f::down();
            
            REQUIRE( v.x == 0 );
            REQUIRE( v.y == -1 );
        }
        
        SECTION("Vector Left"){
            Vector2f v = Vector2f::left();
            
            REQUIRE( v.x == -1 );
            REQUIRE( v.y == 0 );
        }
        
        SECTION("Vector Right"){
            Vector2f v = Vector2f::right();
            
            REQUIRE( v.x == 1 );
            REQUIRE( v.y == 0 );
        }
    }
}

TEST_CASE("Copy constructor"){
    WHEN("Vector2i"){
        Vector2i v1(1, 2);
        Vector2i v2(v1);
        
        REQUIRE(v1.x == v2.x);
        REQUIRE(v1.y == v2.y);
    }
    
    WHEN("Vector2f"){
        Vector2f v1(0.1f, 0.2f);
        Vector2f v2(v1);
        
        REQUIRE(v1.x == v2.x);
        REQUIRE(v1.y == v2.y);
        
    }
}

TEST_CASE("Copy assignment operator"){
    WHEN("Vector2i"){
        Vector2i v1(1, 2);
        Vector2i v2 = v1;
        
        REQUIRE(v1.x == v2.x);
        REQUIRE(v1.y == v2.y);
    }
    
    WHEN("Vector2f"){
        Vector2f v1(0.1f, 0.2f);
        Vector2f v2 = v1;
        
        REQUIRE(v1.x == v2.x);
        REQUIRE(v1.y == v2.y);
        
    }
}

TEST_CASE("math +"){
    WHEN("Vector2i"){
        Vector2i v1(1, 2);
        Vector2i v2 = v1 + 1;
        
        REQUIRE(v2.x == 2);
        REQUIRE(v2.y == 3);
    }
    
    WHEN("Vector2f"){
        Vector2f v1(0.1f, 0.2f);
        Vector2f v2 = v1 + 1;
        
        REQUIRE(v2.x == 1.1f);
        REQUIRE(v2.y == 1.2f);
    }
}

TEST_CASE("math -"){
    WHEN("Vector2i"){
        Vector2i v1(1, 2);
        Vector2i v2 = v1 - 1;
        
        REQUIRE(v2.x == 0);
        REQUIRE(v2.y == 1);
    }
    
    WHEN("Vector2f"){
        Vector2f v1(0.1f, 0.2f);
        Vector2f v2 = v1 - 1;
        
        REQUIRE(v2.x == -0.9f);
        REQUIRE(v2.y == -0.8f);
    }
}

TEST_CASE("math *"){
    WHEN("Vector2i"){
        Vector2i v1(1, 2);
        Vector2i v2 = v1 * 3;
        
        REQUIRE(v2.x == 3);
        REQUIRE(v2.y == 6);
    }
    
    WHEN("Vector2f"){
        Vector2f v1(0.1f, 0.2f);
        Vector2f v2 = v1 *3;
        
        REQUIRE(v2.x == 0.3f);
        REQUIRE(v2.y == 0.6f);
    }
}

TEST_CASE("math /"){
    WHEN("Vector2i"){
        Vector2i v1(2, 4);
        Vector2i v2 = v1 / 2;
        
        REQUIRE(v2.x == 1);
        REQUIRE(v2.y == 2);
    }
    
    WHEN("Vector2f"){
        Vector2f v1(0.1f, 0.2f);
        Vector2f v2 = v1 / 0.1;
        
        REQUIRE(v2.x == 1.0f);
        REQUIRE(v2.y == 2.0f);
    }
}

TEST_CASE("dot product"){
    WHEN("Vector2i"){
        Vector2i v1(2, 4);
        Vector2i v2(1, 2);
        int result = v1.dot(v2);
        
        REQUIRE(result == 10);
    }
    
    WHEN("Vector2f"){
        Vector2f v1(0.1f, 0.2f);
        Vector2f v2(10.0f, 20.0f);
        float result = v1.dot(v2);
        
        REQUIRE(result == 5.0f);
    }
}


TEST_CASE("magnitude"){
    WHEN("Vector2f"){
        Vector2f v(3.0f, 4.0f);
        float result = v.magnitude();
        
        REQUIRE(result == 5.0f);
    }
}

TEST_CASE("magnitude square"){
    WHEN("Vector2f"){
        Vector2f v(3.0f, 4.0f);
        float result = v.magnitude_square();
        
        REQUIRE(result == 25.0f);
    }
}

TEST_CASE("unit vector"){
    WHEN("Vector2f"){
        Vector2f v(3.0f, 4.0f);
        Vector2f result = v.unit();
        
        REQUIRE(result.x == 0.6f);
        REQUIRE(result.y == 0.8f);
        REQUIRE(result.magnitude() == 1.0f);
    }
}

TEST_CASE("Direction"){
    WHEN("Vector2f"){
        Vector2f v(1.0f, 1.0f);
        float angle = v.direction();
        
        REQUIRE(abs(angle - M_PI/4.0f) < 0.001f);
    }
}

TEST_CASE("angle between 2 vectors"){
    WHEN("Vector2f"){
        Vector2f v1(1.0f, 0.0f);
        Vector2f v2(0.0f, 1.0f);
        float angle = v1.angle(v2);
        
        REQUIRE(abs(angle - M_PI/2.0f) < 0.001f);
    }
}

#endif /* vector2_test_h */
