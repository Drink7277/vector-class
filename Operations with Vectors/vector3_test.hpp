//
//  vector3_test.hpp
//  Operations with Vectors
//
//  Created by Drink on 6/7/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef vector3_test_h
#define vector3_test_h

using namespace dk_math;

TEST_CASE("Vector3f Static Method"){
    SECTION("Vector zeros"){
        Vector3f v = Vector3f::zeros();
        
        REQUIRE( v.x == 0 );
        REQUIRE( v.y == 0 );
        REQUIRE( v.z == 0 );
    }
    
    SECTION("Vector ones"){
        Vector3f v = Vector3f::ones();
        
        REQUIRE( v.x == 1 );
        REQUIRE( v.y == 1 );
        REQUIRE( v.z == 1 );
    }
    
    SECTION("Vector up"){
        Vector3f v = Vector3f::up();
        
        REQUIRE( v.x == 0 );
        REQUIRE( v.y == 1 );
        REQUIRE( v.z == 0 );
    }
    
    SECTION("Vector down"){
        Vector3f v = Vector3f::down();
        
        REQUIRE( v.x == 0 );
        REQUIRE( v.y == -1 );
        REQUIRE( v.z == 0 );
    }
    
    SECTION("Vector left"){
        Vector3f v = Vector3f::left();
        
        REQUIRE( v.x == -1 );
        REQUIRE( v.y == 0 );
        REQUIRE( v.z == 0 );
    }
    
    SECTION("Vector right"){
        Vector3f v = Vector3f::right();
        
        REQUIRE( v.x == 1 );
        REQUIRE( v.y == 0 );
        REQUIRE( v.z == 0 );
    }
    
    SECTION("Vector front"){
        Vector3f v = Vector3f::front();
        
        REQUIRE( v.x == 0 );
        REQUIRE( v.y == 0 );
        REQUIRE( v.z == 1 );
    }
    
    SECTION("Vector back"){
        Vector3f v = Vector3f::back();
        
        REQUIRE( v.x == 0 );
        REQUIRE( v.y == 0 );
        REQUIRE( v.z == -1 );
    }
}

TEST_CASE("Vector3f Copy constructor"){
    Vector3f v1(1, 2, 3);
    Vector3f v2(v1);
    
    REQUIRE(v1.x == v2.x);
    REQUIRE(v1.y == v2.y);
    REQUIRE(v1.z == v2.z);
}

TEST_CASE("Vector3f Copy assignment operator"){
    Vector3f v1(0.1f, 0.2f, 0.3f);
    Vector3f v2 = v1;
    
    REQUIRE(v1.x == v2.x);
    REQUIRE(v1.y == v2.y);
    REQUIRE(v1.z == v2.z);
}

TEST_CASE("Vector3f math +"){
    Vector3f v1(0.1f, 0.2f, 0.3f);
    Vector3f v2 = v1 + 1;
    
    REQUIRE(v2.x == 1.1f);
    REQUIRE(v2.y == 1.2f);
    REQUIRE(v2.z == 1.3f);
}

TEST_CASE("Vector3f math -"){
    Vector3f v1(0.1f, 0.2f, 0.3f);
    Vector3f v2 = v1 - 1;
    
    REQUIRE(v2.x == -0.9f);
    REQUIRE(v2.y == -0.8f);
    REQUIRE(v2.z == -0.7f);
}

TEST_CASE("Vector3f math *"){
    Vector3f v1(0.1f, 0.2f, 0.4f);
    Vector3f v2 = v1 * 3;
    
    REQUIRE(v2.x == 0.3f);
    REQUIRE(v2.y == 0.6f);
    REQUIRE(v2.z == 1.2f);
}

TEST_CASE("Vector3f math /"){
    Vector3f v1(0.1f, 0.2f, 0.3f);
    Vector3f v2 = v1 / 0.1;
    
    REQUIRE(v2.x == 1.0f);
    REQUIRE(v2.y == 2.0f);
    REQUIRE(v2.z == 3.0f);
}

TEST_CASE("Vector3f dot product"){
    Vector3f v1(0.1f, 0.2f, 0.3f);
    Vector3f v2(10.0f, 20.0f, 30.0f);
    float result = v1.dot(v2);
    
    REQUIRE(result == 14.0f);
}

TEST_CASE("Vector3f cross product"){
    Vector3f v1(0.1f, 0.2f, 0.3f);
    Vector3f v2(30.0f, 20.0f, 10.0f);
    Vector3f result = v1.cross(v2);
    
    REQUIRE(result.x == -4.0f);
    REQUIRE(result.y == 8.0f);
    REQUIRE(result.z == -4.0f);
}



TEST_CASE("Vector3f magnitude"){
    Vector3f v(2.0f, 3.0f, 4.0f);
    float result = v.magnitude();
    
    REQUIRE(result == 5.385164807134504f);
}

TEST_CASE("Vector3f magnitude square"){
    Vector3f v(2.0f, 3.0f, 4.0f);
    float result = v.magnitude_square();
    
    REQUIRE(result == 29.0f);
}

TEST_CASE("Vector3f unit vector"){
    Vector3f v(2.0f, 3.0f, 4.0f);
    Vector3f result = v.unit();
    
    REQUIRE(abs(result.x - 0.3713906763541037f) < 0.001f );
    REQUIRE(abs(result.y - 0.5570860145311556f) < 0.001f);
    REQUIRE(abs(result.z - 0.7427813527082074f) < 0.001f);
    REQUIRE(result.magnitude() == 1.0f);
}

TEST_CASE("Vector3f angle between 2 vectors"){
    Vector3f v1(0.1f, 0.2f, 0.3f);
    Vector3f v2(30.0f, 20.0f, 10.0f);
    float angle = v1.angle(v2);
    
    REQUIRE(abs(angle - 0.7751933733103613f) < 0.001f);
}


#endif /* vector3_test_h */
